-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Nov 2019 pada 15.49
-- Versi server: 10.3.15-MariaDB
-- Versi PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `modul5`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ead_laravel_post`
--

CREATE TABLE `ead_laravel_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(191) NOT NULL,
  `image` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ead_laravel_users`
--

CREATE TABLE `ead_laravel_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `email_verifed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(191) NOT NULL,
  `title` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `url` int(191) NOT NULL,
  `avatar` varchar(191) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dava', 'davaperdana@gmail.com', NULL, '$2y$10$O7Rkmdnc5wYiK.IcT6U.Lu18VSvBRvCYYBBTyXal/WcfuVxwPkUqa', NULL, '2019-11-02 13:49:30', '2019-11-02 13:49:30'),
(2, 'davunk', 'dava@gmail.com', NULL, '$2y$10$go.Ynz6S9IGtP389TOvpVu8ZSwEfJOzTldZ2Ty4j7dF/SE7CNWuxO', NULL, '2019-11-02 14:29:59', '2019-11-02 14:29:59'),
(3, 'jimi', 'jimi@gmail.com', NULL, '$2y$10$lAVZyhnDKnj9qql0Ibf7FeQvxBqt5A6plbQqxdKYo0nekH0YDpiDu', NULL, '2019-11-02 14:31:12', '2019-11-02 14:31:12'),
(4, 'Dava Aditya', 'davaperdana2211@gmail.com', NULL, '$2y$10$N06FXrvxHtPYLEVX3PnQZevko0GmR3AHHQxtBO/KLXLHwNRumkidi', NULL, '2019-11-02 23:22:26', '2019-11-02 23:22:26'),
(5, 'dava', 'dava2211@gmail.com', NULL, '$2y$10$6qjctiljJIUX.ZpE73PBi.r6WwmuQ6p6TiijR/ELQYkDCpfOTZmbe', NULL, '2019-11-02 23:59:32', '2019-11-02 23:59:32'),
(6, 'veder', 'vedder_persib@yahoo.co.id', NULL, '$2y$10$1B50S8pc9T.HeC7CctwAq.znlLQM88anOU7/HGbtXXw6W7E8OmYyi', NULL, '2019-11-03 00:04:18', '2019-11-03 00:04:18'),
(7, 'davunk', 'davunk@gmail.com', NULL, '$2y$10$ymbumt5XhpGtuO0NP47dHeHrocjva6blgg/1I38mOKhflHSw5T9H6', NULL, '2019-11-03 00:06:01', '2019-11-03 00:06:01'),
(8, 'fiqri', 'fiqri@gmail.com', NULL, '$2y$10$j4AcpIXYgcfsxjDxMQZbr.EVrycrEzRVzh.uCkiXuR7RMjNhnkn56', NULL, '2019-11-03 00:21:16', '2019-11-03 00:21:16'),
(9, 'irfan', 'irfan@gmaill.com', NULL, '$2y$10$o8zhDP93YhZB7Rai/QWdq.b4xDLArJPa0qA0Gp/fWQV7/wIkIbq/e', NULL, '2019-11-03 01:20:01', '2019-11-03 01:20:01');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ead_laravel_post`
--
ALTER TABLE `ead_laravel_post`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ead_laravel_users`
--
ALTER TABLE `ead_laravel_users`
  ADD PRIMARY KEY (`id`,`email`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ead_laravel_post`
--
ALTER TABLE `ead_laravel_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ead_laravel_users`
--
ALTER TABLE `ead_laravel_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
