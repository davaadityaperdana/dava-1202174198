@extends('layouts.app')
@section('content')
<center><h1>Add New Post</h1></center>
<div class="container" style="width:60%">
    <div class="row justify-content-center">     
    <form action="posting">
            <div class="card-body" >
            <div class="form-group">
                <label >Post Caption</label>
                <textarea id="newPost" cols="30" class="form-control" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label >Post Image</label>
                <br>
                <input type="file" id="avatar" >
            </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection