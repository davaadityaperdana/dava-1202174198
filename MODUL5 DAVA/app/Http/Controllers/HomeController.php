<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts= DB::table('posts')->join('users','posts.user_id','=','users.id')->select('posts.*','users.name')->get();
        return view('home',[
            'posts'=>$posts
        ]);
    }
    public function profile(){
        $users = DB::table('users')->select('users.*')->where('id',(Auth::user()->id))->first();
        $posts = DB::table('posts')->join('users','posts.user_id','=','users.id')->select('posts.*','users.*')->get();
    
       return view('profile',['user'=>$users],['posts'=>$posts]);
       
       }
}