<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $table = 'ead_laravel_post';
    protected $fillable = ['user_id','caption','image'];
}
