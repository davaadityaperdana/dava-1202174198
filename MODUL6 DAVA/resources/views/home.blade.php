@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($posts as $e)
            <div class="card">
                <div class="card-header">{{$e->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <img src="{{asset('welcome.jpg')}}" class="img"  width="60%" heigth="30%"><br>

                </div>
            </div>
                @endforeach
        </div>
    </div>
</div>
@endsection