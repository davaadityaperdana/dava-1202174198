@extends('layouts.app')
@section('content')
<center><h1>Edit Profile</h1></center>
<div class="container">
    <div class="row justify-content-center">
        <form method="GET" action="{{ route('edit') }}">
            <div class="form-group">
                <label >Title</label>
                <input type="text" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label >Description</label>
                <input type="text" id="description" class="form-control">
            </div>
            <div class="form-group">
                <label >URL</label>
                <input type="text" id="url" class="form-control">
            </div>
            <div class="form-group">
                <label >Profile Image</label>
                <br>
                <input type="file" id="avatar" >
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection