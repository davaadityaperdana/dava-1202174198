<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;

class profileController extends Controller
{
    public function index()
    {
        $users=DB::table('users')
        ->select('users.*')
        ->where('id',(Auth::user()->id))
        ->first();

        $posts=DB::table('posts')
        ->join('users','posts.user_id','=','users.id')
        ->select('posts.*','users.*')
        ->get();


        return view('profile',['user'=>$users],['post'=>$posts]);
    }
}
